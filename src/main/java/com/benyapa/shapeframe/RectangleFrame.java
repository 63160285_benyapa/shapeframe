/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author bwstx
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(new Color(205, 240, 234));
        frame.setLayout(null);

        JLabel lblWidth = new JLabel("width", JLabel.CENTER);
        lblWidth.setSize(50, 20);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        lblWidth.setLocation(5, 10);
        frame.add(lblWidth);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 10);
        frame.add(txtWidth);

        JLabel lblHeight = new JLabel("height", JLabel.CENTER);
        lblHeight.setSize(50, 20);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        lblHeight.setLocation(5, 35);
        frame.add(lblHeight);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 35);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 20);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle w*h = ?, area = ?, perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    double width = Double.parseDouble(strWidth);
                    double height = Double.parseDouble(strHeight);
                    Rectangle rec = new Rectangle(width, height);
                    lblResult.setText("Rectangle w*h = " + String.format("%.2f", rec.getWidth()) + " * "
                            + String.format("%.2f", rec.getHeight())
                            + " area = " + String.format("%.2f", rec.calArea())
                            + " perimeter = " + String.format("%.2f", rec.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please Input Number", "Error!",
                            JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }

}
