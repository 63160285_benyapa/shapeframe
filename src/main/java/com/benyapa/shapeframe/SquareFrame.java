/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author bwstx
 */
public class SquareFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(new Color(10, 129, 171));
        frame.setLayout(null);

        JLabel lblSide = new JLabel("side", JLabel.CENTER);
        lblSide.setSize(50, 20);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        lblSide.setLocation(5, 10);
        frame.add(lblSide);

        final JTextField txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 10);
        frame.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 10);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Square side = ?, area = ?, perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 90);
        lblResult.setBackground(new Color(249, 223, 220));
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lblResult.setText("Square side = " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please Input Number", "Error! ",
                            JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }

}
