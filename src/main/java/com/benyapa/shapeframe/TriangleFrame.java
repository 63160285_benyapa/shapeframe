/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author bwstx
 */
public class TriangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(new Color(243, 139, 160));
        frame.setLayout(null);

        JLabel lblBase = new JLabel("base", JLabel.CENTER);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 10);
        lblBase.setBackground(Color.white);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 10);
        frame.add(txtBase);

        JLabel lblHeight = new JLabel("height", JLabel.CENTER);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 35);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 35);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 20);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle base, height = ? , area = ? , perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(new Color(237, 246, 229));
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base, height);
                    lblResult.setText("Triangle base, height = " + String.format("%.2f", triangle.getBase())
                            + " , " + String.format("%.2f", triangle.getHeight())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " parimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please Input Number", "Error!",
                            JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                }

            }

        });

        frame.setVisible(true);
    }

}
